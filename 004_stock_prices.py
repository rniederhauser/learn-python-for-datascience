from sklearn.svm import SVR
import matplotlib.pyplot as plt
import numpy as np
import csv

dates, prices = [], [] 

def fetch_data(filename='res/aapl.csv'):
    with open(filename, 'r') as f:
        csv_reader = csv.reader(f)
        next(csv_reader)
        for line in csv_reader:
            dates.append(int(line[0].split("-")[1]))
            prices.append(float(line[1]))

def predict_prices(dates, prices, x):
    dates = np.reshape(dates, (len(dates), 1))

    svr_lin = SVR(kernel='linear', C=1e3)
    svr_poly = SVR(kernel='poly', C=1e3, degree=2)
    # rbf = radio basis function, euclidean distance between 2 inputs  
    # gamma defines how far too far is => too far = 0, else 1
    svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)

    svr_lin.fit(dates, prices)
    svr_poly.fit(dates, prices)
    svr_rbf.fit(dates, prices)

    plt.scatter(dates, prices, color='black', label='Data')
    plt.plot(dates, svr_lin.predict(dates), color='green', label='Linear Model')
    plt.plot(dates, svr_poly.predict(dates), color='blue', label='Polynomial Model')
    plt.plot(dates, svr_rbf.predict(dates), color='red', label='RBF Model')

    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.title('Support Vector Regression')

    plt.legend()
    plt.show()

    return svr_rbf.predict(x)[0], svr_poly.predict(x)[0], svr_lin.predict(x)[0]


fetch_data()
predict_price = predict_prices(dates, prices, 11)
print(predict_price)
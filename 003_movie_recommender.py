"""
Recommendation algorithms helps us make recommendations by learning from past occurrences

There are two distinct types: content-based and collaborative
content-based learns only from a user's data
collaborative learns from other people's data, too

LightFM is a library which allows us to create a recommendation model
"""

import numpy as np
import scipy
from lightfm.datasets import fetch_movielens
from lightfm import LightFM

# fetch data set
data = fetch_movielens(min_rating=4.0)

# print training and testing data
print(repr(data["train"]))
print(repr(data["test"]))

# create model
# warp = weighted approximate-rank pairwise
# warp helps us make recommendations, uses gradient descent
model = LightFM(loss="warp")
# train model
model.fit(data["train"], epochs=30, num_threads=2)

def sample_recommendation(model, data, user_ids):
    # number of users and movies in training data
    n_users, n_items = data["train"].shape 

    # generate recommendations
    for user_id in user_ids:
        # movies they already liked (datatype is a compressed sparse row format)
        known_positives = data["item_labels"][data["train"].tocsr()[user_id].indices]

        # movies the user will like
        scores = model.predict(user_id, np.arange(n_items))
        # rank the scores from most to least liked
        top_items = data["item_labels"][np.argsort(-scores)]

        # Print Out Recommendation
        print(f"User {user_id}")
        print(f"    Known Positives:")
        for x in known_positives[:3]:
            print(f"        {x}")

        print(f"    Recommended:")

        for x in top_items[:3]:
            print(f"        {x}")

sample_recommendation(model, data, [3, 25, 450])
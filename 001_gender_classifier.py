"""
import tree from scikit learn 
which allows us to create decision trees
the decision tree asks the data a yes/no question and thus
determines the classification/category of the data
it will build long branches of yes/no (nodes => classification)
"""
from sklearn import tree
from sklearn import neural_network
from sklearn import naive_bayes

classifiers = {"tree": tree.DecisionTreeClassifier(), 
"neural": neural_network.MLPClassifier(), 
"gaussian": naive_bayes.GaussianNB()}

# [height, weight, shoe size]
X = [
   [181,80,44], [177,70,43], [160,60,38], [154,54,37],
   [166,65,40], [190,90,47], [175,64,39], [177,70,40], [159,55,37],
   [171,75,42], [181,85,43]
]

Y = ['male', 'female', 'female', 'female', 'male', 'male',
    'male', 'female', 'male', 'female', 'male']

sample = [[168, 50, 39]]

for key, classifier in classifiers.items():
    print(key, end=' ')
    classifier = classifier.fit(X, Y)
    print(classifier.predict(sample))
